<?php
header('Content-Type: application/json');
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "testxo";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    echo json_encode([
        'code' => 500,
        'message' => 'กรุณาเชื่อมต่อ db '
    ]);
    die();
}
if(isset($_POST['isEmpty'])){
    $sql = "DELETE FROM replay";
    if ($conn->query($sql) === TRUE) {
        echo json_encode([
            'code' => 200,
            'message' => 'ok'
        ]);
    } else {
        echo json_encode([
            'code' => 301,
            'message' => 'เกิดข้อผิดพลาด กรุณาตรวจสอบว่าเชื่อมต่อ db หรือยัง',
        ]);
        
    }
}else if(isset($_POST['index']) && isset($_POST['char']) && isset($_POST['size'])){
    $index = isset($_POST['index'])?$_POST['index']:'';
    $char = isset($_POST['char'])?$_POST['char']:'';
    $size = isset($_POST['size'])?$_POST['size']:'';
    $sql = "INSERT INTO replay (`index`,`char`,`size`) VALUES ('$index','$char','$size')";
    
    
    if ($conn->query($sql) === TRUE) {
        echo json_encode([
            'code' => 200,
            'message' => 'ok'
        ]);
    } else {
        echo json_encode([
            'code' => 301,
            'message' => 'เกิดข้อผิดพลาด กรุณาตรวจสอบว่าเชื่อมต่อ db หรือยัง',
        ]);
        
    }
}else{
$sql = "SELECT * FROM replay";
    $result = $conn->query($sql);
        /* fetch object array */
    $index = 0;
    $rowcount=mysqli_num_rows($result);
    if($rowcount == 0){
        echo json_encode([
            'code' => 301,
            'message' => 'ไม่มีข้อมูลให้ replay || เกิดข้อผิดพลาด กรุณาตรวจสอบว่าเชื่อมต่อ db หรือยัง',
        ]);
        return false;
    }
    while ($row = $result->fetch_row()) {
        $res[$index] = [
            'index' => $row[0],
            'char' => $row[1],
            'size' => $row[2],
        ];
        $index++;
    }
    echo json_encode([
        'code' => 200,
        'data' => $res
    ]);
}
$conn->close();
?>