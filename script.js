var boxs = [], 
is_size = 3, 
turn = document.getElementById("turn"),
boxes = document.querySelectorAll("#workspace div"), 
X_or_O = 0,
sLoop = 0,
eLoop = 1;

$(document).ready(function(){
    $.post("db.php",{isEmpty :1},function(r){
        if(r.code != 200){
            alert(r.message)
        }
    }).fail(function(response) {
    alert('กรุณาต่อ db ก่อน');
});
})

$(document).on('click', '#workspace div', function(e) {
    if(this.innerHTML !== "X" && this.innerHTML !== "O"){
        if(X_or_O%2 === 0){
            this.innerHTML = "X"; 
            turn.innerHTML = "O Turn Now";
            getWinner();
            X_or_O += 1;
            
        }else{
            this.innerHTML = "O";
            turn.innerHTML = "X Turn Now";
            getWinner();
            X_or_O += 1;  
        }
    }
    insertLog($(this));
});

function changeSize(value){
    is_size = value != null ? value :$('#is_size').val();
    let size_container = 0;
    let sum_size = 0;
    let html = '';
    if(is_size == '' || is_size == null || is_size < 2){
        alert("กรุณาระบุขนาดเกมส์ || กรุณาระบุขนาดที่มากกว่า 1");
        return false;
    }
    boxs = [];
    size_container = (is_size*100);
    $(".container").css("width", size_container);
    sum_size = is_size*is_size; 
    setBoxs(is_size);
    for(let i = 1;i<=sum_size;i++){
        html+='<div class="box" id="'+i+'"></div>';
    };
    $('#workspace').html('');
    $('#workspace').append(html);
    boxes = document.querySelectorAll("#workspace div")
    $.post("db.php",{isEmpty :1},function(r){
        if(r.code != 200){
            alert(r.message)
        }
    })
    
}

async function changeSizeSlow(value){
    is_size = value[0].size;
    sLoop = 0;
    eLoop = value.length;
    let size_container = 0;
    let sum_size = 0;
    let html = '';
    if(is_size == '' || is_size == null || is_size < 2){
        alert("กรุณาระบุขนาดเกมส์");
        return false;
    }
    boxs = [];
    size_container = (is_size*100);
    $(".container").css("width", size_container);
    sum_size = is_size*is_size; 
    setBoxs(is_size);
    for(let i = 1;i<=sum_size;i++){
        html+='<div class="box disabled" id="'+i+'"></div>';
    };
    $('#workspace').html('');
    $('#workspace').append(html);
    boxes = document.querySelectorAll("#workspace div")
    loopRuning(value);
}

function loopRuning(value) {        
  setTimeout(function() {
      $('#'+value[sLoop].index).html(value[sLoop].char);
    sLoop++;                   
    if (sLoop < eLoop) {          
        loopRuning(value);            
    }                      
  }, 1000)
}  

function setBoxs(size){
    for(let i =1;i<=size;i++){
        boxs[i] = i;    
    }
}

function mapWinArea(num) {
    $('#'+num).addClass('win');
    turn.innerHTML = $('#'+num).text() + " Won Congrat";
    turn.style.fontSize = "40px";
    $('.box').addClass("disabled");
}

function getWinner(){
    let langth = boxes.length;
    let size = is_size;
    let check = [];
    let checkReverst = [];
    for(let round =1;round<=size;round++){
        // // 1
        let array_result = [];
        let start = (size*round)-(size-1);
        let end = (size*round);
        let processStart = (size*round)-(size-2);
        let is_success = false;
        array_result.push(processStart-1);
        if(document.getElementById(start).innerHTML !== ""){
            
            is_success = true;
            for(let i = processStart; i<= end; i++){
                if(document.getElementById(start).innerHTML !== document.getElementById(i).innerHTML){
                    is_success = false;
                    break;
                }
                array_result.push(i);
            }
        }
        
        if(is_success){
            array_result.map(mapWinArea)
            break;
        }
        // 1

        // 2
        array_result = [];
        start = round; 
        end = (size*size);
        processStart = ((parseInt(size)+parseInt(round))); 
        is_success = false;
        array_result.push(start);
        if(document.getElementById(start).innerHTML !== ""){
            is_success = true;
            for(let i = processStart; i<= end; i=parseInt(i)+parseInt(size)){
                if(document.getElementById(start).innerHTML !== document.getElementById(i).innerHTML){
                    is_success = false;
                    break;
                }
                array_result.push(i);
            }
        }

        if(is_success){
            array_result.map(mapWinArea)
            break;
        }
        // 2

        // 3
        array_result = [];
        start = 1;
        processStart = (size*round)-(size-round);
        check.push(processStart);
        if(check.length >= size){
            if(document.getElementById(start).innerHTML !== ""){
                is_success = true;
                for (let i = 0; i < check.length; i++) {
                    if(i == 0){
                        continue;
                    }
                    if(document.getElementById(start).innerHTML !== document.getElementById(check[i]).innerHTML){
                        is_success = false;
                        break;
                    }
                }
            }
        }
        if(is_success){
            array_result = check;
            array_result.map(mapWinArea)
            break;
        }
        // 3

        // 4
        array_result = [];
        start = size;
        if(round == 1){
            processStart2 = size;
        }else{
            processStart2 = (parseInt(checkReverst[round-2])+(size-1));
        }
        if(processStart2 <= (size*size)){
            checkReverst.push(processStart2)
        }
        if(checkReverst.length >= size){
            if(document.getElementById(start).innerHTML !== ""){
                is_success = true;
                for (let i = 0; i < checkReverst.length; i++) {
                    if(i == 0){
                        continue;
                    }
                    if(document.getElementById(start).innerHTML !== document.getElementById(checkReverst[i]).innerHTML){
                        is_success = false;
                        break;
                    }
                }
            }
        }
        if(is_success){
            array_result = checkReverst;
            array_result.map(mapWinArea)
            break;
        }
        // 4
        
        if(round == size){
            let drew = true;
            for(let i =1;i<=(parseInt(size)*parseInt(size));i++){
                if(document.getElementById(i).innerHTML === ""){
                    drew = false;
                }
            }
            if(drew){
                turn.innerHTML = "Drew !"
                turn.style.fontSize = "40px";
            }
        }

    }
}

function clearBoard(){
    $.post("db.php",{isEmpty :1},function(r){
        if(r.code != 200){
            alert(r.message)
        }
    })
    for(var i = 0; i < boxes.length; i++){
        boxes[i].classList.remove("win");
        boxes[i].innerHTML = "";
        turn.innerHTML = "Play";
        turn.style.fontSize = "25px";   
    }
    $('.box').removeClass("disabled");
}

function insertLog(data) {
    let index = data.attr('id');
    let char = data.text();
    let size = is_size;
    $.post("db.php", { index :index,char : char,size:size },function(r){
        if(r.code != 200){
            alert(r.message);
        }
    });
}

function replay(){
    $.get("db.php",'',function(r){
        if(r.code == 200){
            changeSizeSlow(r.data)
        }else{
            alert(r.message)
        }
    })
}