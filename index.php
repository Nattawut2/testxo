
<!DOCTYPE html>
<html>
    <head>
        <title>Test XO</title>
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .disabled{
                pointer-events:none
            }
            *{box-sizing: border-box}
            .container{
                width: 300px;
                overflow: hidden;
                margin: 50px auto 0 auto;
            }

            .header{
                width: 100%;
                overflow: hidden;
                line-height: 100px; 
                text-align: center;
            }
            
            .container span{
                width: 100%;
                display: block;
                text-align: center;
                font-family: sans-serif;
                color: #fff;
                font-size: 25px;
                background: #BBE2D7;
            }
            
            .container .box{
                float: left;
                width: 100px;
                height: 100px;
                border: 1px solid #000;
                transition: all .25s ease-in-out;
                font-family: sans-serif; 
                font-size: 85px;
                text-align: center;
                line-height: 100px; 
                cursor: pointer;
            }
            
            .container .box:hover{
                background: rgba(10,10,10,0.5);
                color: #fff
            }
            
            button{
                background: #BBE2D7;
                color: #fff;
                font-weight: bold;
                border: 1px solid yellow;
                cursor: pointer; 
                width: 200px;
                height: 40px; 
                font-size: 22px;
                display: block;
                margin: 10px auto
            }
            .buttonOk {
                width: 50px;
                height: 25px;
                background: #BBE2D7;
                padding: 10px;
                text-align: center;
                border-radius: 5px;
                color: white;
                font-weight: bold;
                line-height: 25px;
            }

            .win{background: #F7D07A; color: #fff}
        </style>
    </head>
    <body>
    <div class="header">
        <label> ขนาด</label>
        <input type="number" name="is_size" id="is_size">
        <a onclick="changeSize()" class="buttonOk">
            ตกลง
        </a>
    </div>
    <div class="container" id="main">
        
        <span id="turn">Play</span>
        <!-- show X or O on div click -->
        <div id="workspace">
            <div class="box" id="1"></div>
            <div class="box" id="2"></div>
            <div class="box" id="3"></div>
            <div class="box" id="4"></div>
            <div class="box" id="5"></div>
            <div class="box" id="6"></div>
            <div class="box" id="7"></div>
            <div class="box" id="8"></div>
            <div class="box" id="9"></div>
        </div>
    </div>
    <button onclick="replay()">Replay</button>
    <button onclick="clearBoard()">Play Again</button>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="./script.js"></script>